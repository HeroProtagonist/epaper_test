# frozen_string_literal: true

require_relative "epaper/version"
require_relative "epaper/epaper"

module Epaper
  class Error < StandardError; end

  module_function

  def display_image(file_name:)
    Epaper.init

    image = ImageList.new(file_name)

    image.flop!
    image.scale!(800, 480)
    image.rotate!(180)

    # check and convert if not bmp
    blob = image.to_blob.each_byte.map { |byte| byte ^ 255 }.pack('C*')

    start =  blob.length - 48_000

    Epaper.render_image(blob[start..])
  end
end
