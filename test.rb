require 'rmagick'

require 'epaper'

include Magick

# image = ImageList.new('/home/pi/epaper_test/ext/img.bmp')

Epaper.init
sleep(2)

# file_name_1 = '/Users/jordan/Desktop/epaper/ext/pic/7in5_v2.bmp'

# image_1 = ImageList.new(file_name)
# image_1.rotate!(180)
# image_1.flop!

# blob = image_1.to_blob[62..] # 2 bytes less than filesize - why

# str = blob.each_byte.map { |b| b ^ 255 }.pack('C*');

file_name = './trooper.jpeg'

image = ImageList.new(file_name)

image.flop!
image.scale!(800, 480)
image.rotate!(180)

bmp, = Magick::Image.from_blob(image.to_blob { |attrs| attrs.format = 'bmp' })

bmp = bmp.quantize(2, Magick::GRAYColorspace)

blob = bmp.to_blob.each_byte.map { |b| b ^ 255 }.pack('C*')

start =  blob.length - 48_000

Epaper.render_image(blob[start..])

sleep(2)
Epaper.exit

# curl -L https://get.rvm.io | bash
# source /home/pi/.rvm/scripts/rvm
# rvm install 2.7.0

# sudo apt-get install libmagickwand-dev
# gem install rmagick

# rvmsudo ruby rb.rb

# require 'rmagick'
# require './epaper'
# include Magick


# size = File.size(file_name)
# io = File.binread(file_name, size, 64);
# my_method(io.each_byte.map { |b| b ^ 255 }.pack('C*'))

# GC.start

# require 'rmagick'
# include Magick
# file_name = '/Users/jordan/Desktop/epaper-testing/ext/trooper.jpeg'

# file_name = '/Users/jordan/Desktop/epaper-testing/ext/pic/7in5_v2.bmp'

# image = ImageList.new(file_name)
# image.resize!(800, 480)
# new_image = image.quantize(256, Magick::GRAYColorspace)
# image.first.write("ed.bmp")
